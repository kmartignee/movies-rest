FROM node

WORKDIR /usr/movies-app

COPY *.json .
COPY index.js .

RUN npm install

ENTRYPOINT [ "node" ]
CMD [ "index.js" ]
