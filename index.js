const express = require("express");
const { Sequelize, DataTypes } = require("sequelize");

const sequelize = new Sequelize(
  "movies-db",
  "killian",
  "pass",
  {
    host: "movies-mysql",
    port: 3306,
    dialect: "mysql",
  }
);

const Movie = sequelize.define("Movie", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
  },
  title: DataTypes.STRING,
  director: DataTypes.STRING,
  creationDate: DataTypes.DATE
});

(async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
    await Movie.sync();
    await Movie.findOrCreate({
      where: { id: 1 },
      defaults: {
        title: "The Truman Show",
        director: "Peter Weir",
        creationDate: Date.parse("October 28, 1998")
      },
    });
    await Movie.findOrCreate({
      where: { id: 2 },
      defaults: {
        title: "The Shawshank Redemption",
        director: "Frank Darabont",
        creationDate: Date.parse("Mars 1, 1995")
      },
    });
    await Movie.findOrCreate({
      where: { id: 3 },
      defaults: {
        title: "Shutter Island",
        director: "Martin Scorsese",
        creationDate: Date.parse("Febuary 24, 2010")
      },
    });
    console.log("Database initialized successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
})();

const app = express();
const port = 8080;

app.get("/", (req, res) => {
  Movie.findAll().then(r => res.send(r));
});

app.listen(port, () => {
  return console.log(`Server is listening at http://localhost:${port}`);
});
